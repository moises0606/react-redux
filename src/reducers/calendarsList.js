import {
    CALENDAR_SET_PAGE,
    CALENDAR_MAIN_PAGE_LOADED,
    CALENDAR_MAIN_PAGE_UNLOADED
  } from '../constants/actionTypes';
  
  export default (state = {}, action) => {
    switch (action.type) {
      case CALENDAR_SET_PAGE:
        return {
          ...state,
          calendars: action.payload,
          currentPage: action.page
        };
      case CALENDAR_MAIN_PAGE_LOADED:
        return {
          ...state,
          pager: action.pager,
          currentPage: 0,
          tab: action.tab,
          calendars: action.payload,
          currentType: action.currenType
        };
      case CALENDAR_MAIN_PAGE_UNLOADED:
        return {};
      default:
        return state;
    }
  };