import {
    CONTACT,
    UPDATE_FIELD_CONTACT,
    CONTACT_PAGE_UNLOADED,
    ASYNC_START
  } from '../constants/actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case CONTACT:
      return {
        ...state,
        inProgress: false,
        errors: action.error ? action.payload.errors : null
      };
    case CONTACT_PAGE_UNLOADED:
      return {};
    case ASYNC_START:
      if (action.subtype === CONTACT) {
        return { ...state, inProgress: true };
      }
      break;
    case UPDATE_FIELD_CONTACT:
      let err= {}
      
      if (state.errors) {
        err = state.errors;
      }

      switch (action.key) {
        case "email":
          let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          if (!re.test(String(action.value).toLowerCase())) {
            err.email="must be valid"
          } else {
            err.email="valid";
          }
          break;
        case "subject":
          if (action.value.length < 5) {
            err.subject="must be greater than 5 caracters"
          } else {
            err.subject="valid";
          }
          break;
        case "comment":
          if (action.value.length < 20) {
            err.comment="must be greater than 20 caracters"
          }else {
            err.comment="valid";
          }
          break;
        }
      return { ...state, [action.key]: action.value, errors: err };
    default:
      return state;
  }

  return state;
};
