import {
    CALENDAR_PAGE_LOADED,
    CALENDAR_PAGE_UNLOADED
  } from '../constants/actionTypes';
  
  export default (state = {}, action) => {
    switch (action.type) {
      case CALENDAR_PAGE_LOADED:
        return {
          ...state,
          calendar: action.payload.calendar,
        };
      case CALENDAR_PAGE_UNLOADED:
        return {};
      default:
        return state;
    }
  };
  