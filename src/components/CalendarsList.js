import CalendarPreview from './CalendarPreview';
import ListPagination from './ListPagination';
import React from 'react';

const CalendarList = props => {
  if (!props.calendars) {
    return (
      <div className="calendar-preview">Loading...</div>
    );
  }

  if (props.calendars.length === 0) {
    return (
      <div className="calendar-preview">
        No calendars are here... yet.
      </div>
    );
  }

  return (
    <div className="ccomponent-container">
      <div className="calendars-container">
        {
          props.calendars.calendars.map(calendar => {
            return (
              <CalendarPreview calendar={calendar} key={calendar.slug} />
            );
          })
        }
      </div>

      <ListPagination
        calendarsCount={props.calendars.calendarsCount}/>
    </div>
  );
};

export default CalendarList;
