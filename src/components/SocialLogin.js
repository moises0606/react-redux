import React from 'react'
import GoogleLogin from 'react-google-login';
import { connect } from 'react-redux';
import {toastr} from 'react-redux-toastr';
import agent from '../agent';
import {
    LOGIN,
  } from '../constants/actionTypes';

const mapStateToProps = state => ({ ...state.auth });

const mapDispatchToProps = dispatch => ({
    onSubmit: (email, password, username, social=true) =>
      dispatch({ type: LOGIN, payload: agent.Auth.login(email, password, username, social) })
});

class GoogleButton extends React.Component {
    constructor() {
        super();
        this.responseGoogle = (response) => {
            if (response.profileObj) {
                let email = /*'moises@gmail.com'*/response.profileObj.email;
                let id = response.profileObj.googleId;
                let username = response.profileObj.name.replace(/ /g, "");
                this.props.onSubmit(email, id, username);
                toastr.success('Success', 'Login is succesfull')
            }else {
                toastr.error('Error', 'Something was wrong')
                console.warn('HA HABIDO ALGUN ERROR');
            }
        }
    }

    render () {

        console.log(this);
        return (
            <GoogleLogin
                clientId='772032802523-ipdrbvef8fppriifiq4l644qj9fv8dlu.apps.googleusercontent.com'
                onSuccess={this.responseGoogle}
                onFailure={this.responseGoogle}>                  
                <img className="socialbtn" src="/img/google_icon.png"/>
            </GoogleLogin>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GoogleButton);
