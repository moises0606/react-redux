import ListErrors from './ListErrors';
import React from 'react';
import agent from '../agent';
import { connect } from 'react-redux';
import {toastr} from 'react-redux-toastr';
import {
  UPDATE_FIELD_CONTACT,
  CONTACT,
  CONTACT_PAGE_UNLOADED
} from '../constants/actionTypes';

const mapStateToProps = state => ({ ...state.contact });

const mapDispatchToProps = dispatch => ({
  onChangeEmail: value =>
    dispatch({ type: UPDATE_FIELD_CONTACT, key: 'email', value }),
  onChangeSubject: value =>
    dispatch({ type: UPDATE_FIELD_CONTACT, key: 'subject', value }),
  onChangeComment: value =>
    dispatch({ type: UPDATE_FIELD_CONTACT, key: 'comment', value }),
  onSubmit: contactForm => {
    dispatch({ type: CONTACT, payload: agent.Contact.submitContact(contactForm) })
    toastr.success('Success', 'The email, has been send')
  },
  onUnload: () =>
    dispatch({ type: CONTACT_PAGE_UNLOADED })
});

class Contact extends React.Component {
  constructor() {
    super();
    this.changeEmail = ev => this.props.onChangeEmail(ev.target.value);
    this.changeSubject = ev => this.props.onChangeSubject(ev.target.value);
    this.changeComment = ev => this.props.onChangeComment(ev.target.value);
    this.submitForm = (email, subject, comment) => ev => {
      ev.preventDefault();
      let contactForm = {email:email, subject:subject, comment:comment};
      this.props.onSubmit(contactForm);
    }
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    const email = this.props.email;
    const subject = this.props.subject;
    const comment = this.props.comment;

    return (
      <div className="auth-page">
        <div className="container page">
          <div className="row">

            <div className="col-md-6 offset-md-3 col-xs-12">
              <h1 className="text-xs-center">Contact</h1>

              <ListErrors errors={this.props.errors} />

              <form onSubmit={this.submitForm(email, subject, comment)}>
                <fieldset>

                  <fieldset className="form-group">
                    <input
                      className="form-control form-control-lg"
                      type="email"
                      placeholder="Email"
                      name="email"
                      value={this.props.email}
                      onChange={this.changeEmail} />
                  </fieldset>

                  <fieldset className="form-group">
                    <input
                      className="form-control form-control-lg"
                      type="text"
                      name="subject"
                      placeholder="Subject"
                      value={this.props.subject}
                      onChange={this.changeSubject} />
                  </fieldset>

                  <fieldset className="form-group">
                    <input
                      className="form-control form-control-lg"
                      type="text"
                      name="comment"
                      placeholder="Comment"
                      value={this.props.comment}
                      onChange={this.changeComment} />
                  </fieldset>

                  <button
                    className="btn btn-lg btn-primary pull-xs-right"
                    type="submit"
                    disabled={this.props.inProgress}>
                    Sign up
                  </button>

                </fieldset>
              </form>
            </div>

          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Contact);
