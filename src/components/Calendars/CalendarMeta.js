import CalendarActions from './CalendarActions';
import { Link } from 'react-router-dom';
import React from 'react';

const CalendarMeta = props => {
  const calendar = props.calendar;
  return (
    <div className="calendar-meta">
      <Link to={`/@${calendar.author.username}`}>
        <img src={calendar.author.image} alt={calendar.author.username} />
      </Link>

      <div className="info">
        <Link to={`/@${calendar.author.username}`} className="author">
          {calendar.author.username}
        </Link>
        <span className="date">
          {new Date(calendar.createdAt).toDateString()}
        </span>
      </div>

      <CalendarActions canModify={props.canModify} calendar={calendar} />
    </div>
  );
};

export default CalendarMeta;
