import React from 'react';
import agent from '../../agent';
import { connect } from 'react-redux';
import CalendarMeta from './CalendarMeta';
import { CALENDAR_PAGE_LOADED, CALENDAR_PAGE_UNLOADED } from '../../constants/actionTypes';

const mapStateToProps = state => ({
  ...state.calendar,
  currentUser: state.common.currentUser
});

const mapDispatchToProps = dispatch => ({
  onLoad: payload =>
    dispatch({ type: CALENDAR_PAGE_LOADED, payload }),
  onUnload: () =>
    dispatch({ type: CALENDAR_PAGE_UNLOADED })
});

class Calendar extends React.Component {
  componentWillMount() {
    this.props.onLoad(
        agent.Calendars.get(this.props.match.params.slug)
    )
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
      if (!this.props.calendar) {
        return (
          <div className="calendar-preview">Loading...</div>
        );
      }

      const canModify = this.props.currentUser && this.props.currentUser.username === this.props.calendar.author.username;
      const horario = JSON.parse(this.props.calendar.normalTime.replace(/ /g, "").replace(/'/g, `"`));

      return (
        <div className="calendar-page">
        
        <div className="banner">
          <div className="container">

            <h1>Calendario {this.props.calendar.name}</h1>
            <CalendarMeta
              calendar={this.props.calendar}
              canModify={canModify} />

          </div>
        </div>

        <div className="container page">
            <h1>Puedes solicitar cita en estas horas: </h1>
            <h2>Por la mañana a las {horario.morning.start}-{horario.morning.end}</h2>
            <h2>Por la tarde a las {horario.evening.start}-{horario.evening.end}</h2>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Calendar);
