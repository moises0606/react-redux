import React from 'react';
import { Link } from 'react-router-dom';
import agent from '../agent';
import { connect } from 'react-redux';
import { CALENADAR_SUSCRIBED, CALENADAR_UNSUSCRIBED } from '../constants/actionTypes';

// const SUSCRIBED_CLASS = 'btn btn-sm btn-primary';
// const NOT_SUSCRIBED_CLASS = 'btn btn-sm btn-outline-primary';

const mapDispatchToProps = dispatch => ({
  suscribed: slug => dispatch({
    type: CALENADAR_SUSCRIBED,
    payload: agent.Calendars.suscribed(slug)
  }),
  unsuscribed: slug => dispatch({
    type: CALENADAR_UNSUSCRIBED,
    payload: agent.Calendars.unsuscribed(slug)
  })
});

const CalendarPreview = props => {
  const calendar = props.calendar;

  return (
      <div className="calendar-preview">
        <div className="calendar-meta">
          <div className="info">
            <Link to={`/@${calendar.author.username}`}>
              <img src={calendar.author.image} alt={calendar.author.username} />
            </Link>

            <Link className="author" to={`/@${calendar.author.username}`}>
              {calendar.author.username}
            </Link>

            <span className="date">
              {new Date(calendar.createdAt).toDateString()}
            </span>
          </div>

        </div>

        <Link to={`/calendar/${calendar.slug}`} className="preview-link">
          <h1>{calendar.name}</h1>
          <span>Read more...</span>
        </Link>
      </div>
  );
}

export default connect(() => ({}), mapDispatchToProps)(CalendarPreview);
