import React from 'react';
import agent from '../agent';
import { connect } from 'react-redux';
import {
  CALENDAR_MAIN_PAGE_LOADED,
  CALENDAR_MAIN_PAGE_UNLOADED
} from '../constants/actionTypes';
import CalendarList from './CalendarsList';

const Feed = props => {
  if (props.token) {
    const clickHandler = ev => {
      ev.preventDefault();
      props.onTabClick('feed', agent.Calendars.feed, agent.Calendars.feed());
    }

    return (
      <li className="nav-item">
        <a  href=""
            className={ props.tab === 'feed' ? 'nav-link active' : 'nav-link' }
            onClick={clickHandler}>
          Your Feed
        </a>
      </li>
    );
  }
  return null;
};

const All = props => {
  const clickHandler = ev => {
    ev.preventDefault();
    props.onTabClick('all', agent.Calendars.all, agent.Calendars.all());
  };
  return (
    <li className="nav-item">
      <a
        href=""
        className={ props.tab === 'all' ? 'nav-link active' : 'nav-link' }
        onClick={clickHandler}>
        Global Feed
      </a>
    </li>
  );
};


const mapStateToProps = state =>
  ({
    ...state.calendarsList,
    token: state.common.token
  });

const mapDispatchToProps = dispatch => ({
  onLoad: (tab, pager, payload, currenType) =>
    dispatch({ type: CALENDAR_MAIN_PAGE_LOADED, tab, pager, payload:payload, currenType}),
  onUnload: () =>
    dispatch({  type: CALENDAR_MAIN_PAGE_UNLOADED })
});

class CalendarMain extends React.Component {
  componentWillMount(){
    const tab = this.props.token ? 'feed' : 'all';
    const choosedType = this.props.type? agent.Calendars[this.props.type]:1;
    const calendarsPromise = this.props.token ?
      agent.Calendars.feed :
      agent.Calendars.all;

    this.props.onLoad(tab, this.props.type ? choosedType:calendarsPromise, this.props.type ? choosedType():calendarsPromise(), this.props.type ? this.props.type:undefined);
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  navigation() {
    return (
      <div className="feed-toggle">
        <ul className="nav nav-pills outline-active">

          <Feed
            token={this.props.token}
            tab={this.props.tab}
            onTabClick={this.props.onLoad} />

          <All tab={this.props.tab} onTabClick={this.props.onLoad} />
        
        </ul>
      </div>
    )
  }
  render() {
    return (
      <div className="calendar-page">
        <div className="container page">
          
          {
          /**
           * Call this function and if we come from profile don't show the
           * navigation and check if the current types is the same that we recived
           * if it's different call to the function componentWillMount 
           */
          !this.props.type ? this.navigation():(this.props.type != this.props.currentType) ? this.componentWillMount():""

          }
          <div className="row">
            <CalendarList 
            calendars={this.props.calendars}/>
            
          </div>
        </div>

      </div>
      
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CalendarMain);
