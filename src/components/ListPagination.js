import React from 'react';
import agent from '../agent';
import { connect } from 'react-redux';
import { 
  SET_PAGE, 
  CALENDAR_SET_PAGE 
} from '../constants/actionTypes';

const mapStateToProps = state => 
  ({
    ...state,
    token: state.common.token
  });

const mapDispatchToProps = dispatch => ({
  onSetPage: (page, payload) =>
    dispatch({ type: SET_PAGE, page, payload }),
  onSetPageCalendars: (page, payload) =>
    dispatch({ type: CALENDAR_SET_PAGE, page, payload })
});

const ListPagination = props => {
  if (props.articlesCount <= 10) {
    return null;
  }

  const range = [];
  if (props.articlesCount) {
    for (let i = 0; i < Math.ceil(props.articlesCount / 10); ++i) {
      range.push(i);
    }
  }
  if (props.calendarsCount) {
    for (let i = 0; i < Math.ceil(props.calendarsCount / 10); ++i) {
      range.push(i);
    }
  }

  const setPage = page => {
    if ((props.calendarsList.pager) && (props.router.location.pathname == "/calendars")){
      props.onSetPageCalendars(page, props.calendarsList.pager(page));
    } else {
      if(props.pager) {
        props.onSetPage(page, props.pager(page));
      }else {
        props.onSetPage(page, agent.Articles.all(page))
      }
    }
  };

  const currentPage = (() => {
    if (props.currentPage != undefined) {
      return props.currentPage;
    } 
    if ((props.calendarsList.currentPage != undefined) && (props.router.location.pathname == "/calendars")){
      return props.calendarsList.currentPage;
    }
    return undefined;
  })

  return (
    <nav>
      <ul className="pagination">

        {
          range.map(v => {
            const isCurrent = v === currentPage();
            const onClick = ev => {
              ev.preventDefault();
              setPage(v);
            };
            return (
              <li
                className={ isCurrent ? 'page-item active' : 'page-item' }
                onClick={onClick}
                key={v.toString()}>

                <a className="page-link" href="">{v + 1}</a>

              </li>
            );
          })
        }

      </ul>
    </nav>
  );
};

export default connect(() => mapStateToProps, mapDispatchToProps)(ListPagination);
